import os
import google.generativeai as genai

from dotenv import load_dotenv
load_dotenv()


GEMINI_API_KEY = os.getenv("API_KEY")
genai.configure(api_key=GEMINI_API_KEY)

model = genai.GenerativeModel('gemini-pro')


with open("data.txt", "r") as file:
    my_text = file.read()

response = model.generate_content(my_text)

with open("content.txt", "w") as file:
    file.write(response.text)
